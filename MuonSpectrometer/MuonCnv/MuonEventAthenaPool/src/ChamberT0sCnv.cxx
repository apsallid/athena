/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

#include "MuonChamberT0s/ChamberT0s.h"
#include "ChamberT0sCnv.h"
#include "MuonEventTPCnv/MuonChamberT0s/ChamberT0s_p1.h"

ChamberT0s_PERS* ChamberT0sCnv::createPersistent(Muon::ChamberT0s* transObj) {
    using namespace Muon;
    MsgStream log(msgSvc(), "MuonChamberT0sConverter" );
    ChamberT0s_PERS *persObj = m_converter.createPersistent( transObj, log );
//    if (log.level() <= MSG::DEBUG) log << MSG::DEBUG << "ChamberT0s write Success" << endmsg;
    return persObj;
}
   
Muon::ChamberT0s* ChamberT0sCnv::createTransient() {
    using namespace Muon;
    static const pool::Guid   p1_guid("67E3F1AE-6254-4B29-8D61-5F17D0C19BB2");
    if( compareClassGuid(p1_guid) ) {
        // using unique_ptr ensures deletion of the persistent object
        std::unique_ptr< ChamberT0s_p1 > col_vect( poolReadObject< ChamberT0s_p1 >() );
        MsgStream log(msgSvc(), "MuonChamberT0sConverter_p1" );
        //log << MSG::DEBUG << "Reading ChamberT0s_p1" << endmsg;
        return m_converter.createTransient( col_vect.get(), log );
    } 
    throw std::runtime_error("Unsupported persistent version of ChamberT0s");
}
